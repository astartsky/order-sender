<?php
namespace Astartsky\OrderSender;

use Astartsky\OrderSender\Call\CallInterface;
use Astartsky\OrderSender\Call\RespondInterface;
use Astartsky\OrderSender\Exception\ApiException;

class OrderSender
{
    protected $url;
    protected $callChannel;

    /**
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @return CallChannel
     */
    protected function getCallChannel()
    {
        if (null === $this->callChannel) {
            $this->callChannel = new CallChannel($this->url);
        }

        return $this->callChannel;
    }

    /**
     * @param CallInterface $call
     * @return RespondInterface
     * @throws ApiException
     */
    public function makeCall(CallInterface $call)
    {
        $respond = $this->getCallChannel()->makeCall($call);

        if ($error = $respond->getError()) {
            throw new ApiException($respond->getError(), array(
                "xml" => $call->createRequest(),
                "error" => $error
            ));
        }

        return $respond;
    }
}