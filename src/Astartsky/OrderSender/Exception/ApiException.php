<?php
namespace Astartsky\OrderSender\Exception;

class ApiException extends \Exception
{
    /**
     * @param string $message
     * @param string[] $context
     */
    public function __construct($message, $context = array())
    {
        $this->context = $context;
        parent::__construct($message);
    }

    /**
     * @return string[]
     */
    public function getContext()
    {
        return $this->context;
    }
}