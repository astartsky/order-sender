<?php
namespace Astartsky\OrderSender;

class EscapingTool
{
    /**
     * @param string $value
     * @return string
     */
    public function escape($value)
    {
        return htmlentities($value, ENT_QUOTES, 'UTF-8');
    }
}