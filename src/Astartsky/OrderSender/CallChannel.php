<?php
namespace Astartsky\OrderSender;

use Astartsky\OrderSender\Call\CallInterface;
use Astartsky\OrderSender\Call\RespondInterface;
use Astartsky\OrderSender\Exception\CallException;

class CallChannel
{
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @param CallInterface $call
     * @return RespondInterface
     * @throws Exception\CallException
     * @throws Exception\ApiException
     */
    public function makeCall(CallInterface $call)
    {
        $curl = curl_init($this->url);
        if (false === $curl) {
            throw new CallException("Can't initialize curl");
        }

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "request=" . $call->createRequest());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Expect:"));

        $text = curl_exec($curl);
        if (false === $text) {
            throw new CallException("Can't execute curl: " . curl_error($curl));
        }

        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($text);

        if (false === $xml) {
            $error = libxml_get_last_error();
            throw new CallException("Can't parse xml: " . $error->message, array(
                "xml" => $text
            ));
        }

        $respond = $call->createRespond($xml);

        return $respond;
    }
}