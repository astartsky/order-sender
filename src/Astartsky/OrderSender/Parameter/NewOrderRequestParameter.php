<?php
namespace Astartsky\OrderSender\Parameter;

class NewOrderRequestParameter
{
    protected $client;
    protected $info;
    protected $partner;
    protected $referer;
    protected $order;

    /**
     * @param ClientParameter $client
     * @return $this
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return ClientParameter
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param InfoParameter $info
     * @return $this
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return InfoParameter
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param OrderParameter $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return OrderParameter
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param PartnerParameter $partner
     * @return $this
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return PartnerParameter
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param RefererParameter $referer
     * @return $this
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * @return RefererParameter
     */
    public function getReferer()
    {
        return $this->referer;
    }
}