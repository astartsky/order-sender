<?php
namespace Astartsky\OrderSender\Parameter;

class OrderParameter
{
    protected $items = array();

    /**
     * @param ItemParameter $item
     * @return $this
     */
    public function addItem(ItemParameter $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * @return ItemParameter[]
     */
    public function getItems()
    {
        return $this->items;
    }
}