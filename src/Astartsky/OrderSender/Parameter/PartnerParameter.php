<?php
namespace Astartsky\OrderSender\Parameter;

class PartnerParameter
{
    protected $pid;

    /**
     * @return string
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param string $partner
     * @return $this
     */
    public function setPid($partner)
    {
        $this->pid = $partner;

        return $this;
    }
}