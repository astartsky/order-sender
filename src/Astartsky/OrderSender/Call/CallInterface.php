<?php
namespace Astartsky\OrderSender\Call;

interface CallInterface
{
    /**
     * @return string
     */
    public function createRequest();

    /**
     * @param \SimpleXMLElement $xml
     * @return RespondInterface
     */
    public function createRespond(\SimpleXMLElement $xml);
}