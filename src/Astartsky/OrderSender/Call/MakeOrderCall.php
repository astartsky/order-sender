<?php
namespace Astartsky\OrderSender\Call;

use Astartsky\OrderSender\EscapingTool;
use Astartsky\OrderSender\Parameter\NewOrderRequestParameter;

class MakeOrderCall implements CallInterface
{
    protected $request;
    protected $escape;

    /**
     * @param NewOrderRequestParameter $request
     */
    public function __construct(NewOrderRequestParameter $request)
    {
        $this->request = $request;
        $this->escape = new EscapingTool();
    }

    /**
     * @return string
     */
    public function createRequest()
    {
        $xml = '<?xml version="1.0" encoding="utf-8"?><request>';

        if ($client = $this->request->getClient()) {
            $xml .= '<client>';
            if ($name = $client->getName()) {
                $xml .= '<name>' . $this->escape->escape($name) . '</name>';
            }
            if ($phone = $client->getPhone()) {
                $xml .= '<phone>' . $this->escape->escape($phone) . '</phone>';
            }
            if ($index = $client->getIndex()) {
                $xml .= '<post_index>' . $this->escape->escape($index) . '</post_index>';
            }
            if ($email = $client->getEmail()) {
                $xml .= '<email>' . $this->escape->escape($email) . '</email>';
            }
            if ($address = $client->getAddress()) {
                $xml .= '<address>' . $this->escape->escape($address) . '</address>';
            }
            $xml .= '</client>';
        }

        if ($info = $this->request->getInfo()) {
            $xml .= '<info>';
            if ($paymentMethod = $info->getPaymentMethod()) {
                $xml .= '<payment_method>' . $this->escape->escape($paymentMethod) . '</payment_method>';
            }
            if ($comment = $info->getComment()) {
                $xml .= '<comment>' . $this->escape->escape($comment) . '</comment>';
            }
            $xml .= '</info>';
        }

        if ($partner = $this->request->getPartner()) {
            $xml .= '<partner>'.$this->escape->escape($partner->getPid()).'</partner>';
        }

        if ($referer = $this->request->getReferer()) {
            $xml .= '<from_site>'.$this->escape->escape($referer->getName()).'</from_site>';
        }

        if ($order = $this->request->getOrder()) {
            $xml .= '<order>';
            foreach ($order->getItems() as $item) {
                $xml .= '<item article="'.$this->escape->escape($item->getArticle()).'" size_article="" quantity="'.$this->escape->escape($item->getQuantity()).'" />';
            }
            $xml .= '</order>';
        }

        $xml .= '</request>';

        return $xml;
    }

    /**
     * @param \SimpleXMLElement $xml
     * @return MakeOrderRespond
     */
    public function createRespond(\SimpleXMLElement $xml)
    {
        return MakeOrderRespond::create($xml);
    }
}